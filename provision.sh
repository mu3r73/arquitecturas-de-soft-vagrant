#!/usr/bin/env bash

# script de aprovisionamiento para box: ubuntu/bionic64

# ruta donde nginx va a buscar el sitio web para servir
NGINX_ROOT_PATH="/var/www/html"
# ruta donde vamos a crear un archivo para marcar si ya se aprovisionó
PROVISIONED_PATH="$NGINX_ROOT_PATH/PROVISIONED"

if [[ -f $PROVISIONED_PATH ]]; then
  # ya se aprovisionó => no hace nada
  echo "esta vm ya ha sido aprovisionada..."

else
  # todavía no se aprovisionó
  echo "aprovisionando..."
  
  # actualiza DB de paquetes e instala nginx
  echo "instalando nginx..."
  apt update 
  apt install -y nginx
  INSTALLED=$?
  if ! [[ $INSTALLED -eq 0 ]]; then
    echo "ERROR: no se pudo instalar nginx"
    exit
  fi
  
  # se instaló nginx exitosamente

  # crea directorio para nginx (si no existe), y clona repo de la materia
  echo "clonando repositorio de la materia..."
  rm -r $NGINX_ROOT_PATH
  mkdir $NGINX_ROOT_PATH
  git clone https://github.com/chrodriguez/arquitecturas-de-software.git $NGINX_ROOT_PATH
  SUCCESS=$?
  if ! [[ $SUCCESS -eq 0 ]]; then
    echo "ERROR: no se pudo clonar el repositorio"
    exit
  fi
  
  # se clonó exitosamente el repo
  
  # crea archivo para marcar que ya se aprovisionó
  touch $PROVISIONED_PATH  
fi

echo "... listo"
